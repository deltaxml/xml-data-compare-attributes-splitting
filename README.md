# Attributes Splitting

For details about this sample see our [web pages.](https://docs.deltaxml.com/xml-data-compare/latest/attribute-splitting)

Using the file config-attributes-splitting.xml will disable the default attribute splitting.

Instead, XPath specified on dcf:location points to an attribute (e.g. /persons/@address) which value will be split.

Using local setting for attributes and text splitting allows to choose a different separator and output representation for every attribute in a file.


## REST request:

Replace {LOCATION} below with your location of the download.

```
<compare>
    <inputA xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/attributesTextSplittingA.xml</path>
    </inputA>
    <inputB xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/attributesTextSplittingB.xml</path>
    </inputB>
    <configuration xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/config-attributes-splitting.xml</path>
    </configuration>
</compare>
```

See [XML Data Compare REST User Guide](https://docs.deltaxml.com/xml-data-compare/latest/rest-user-guide) for how to run the comparison using REST.